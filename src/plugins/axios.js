"use strict";

import Vue from 'vue';
import axios from "axios";

// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
//axios配置..
let config = {
  // baseURL:'http://musicapi.leanapp.cn/',
  baseURL:"https://music.kele8.cn/",
  //  baseURL:"  http://localhost:3000",
  // baseURL:'http://www.hjmin.com',
  // baseURL:"http://192.168.94.1:3000",
  //  this.$axios.get('abc/') 最终拼接结果 http://musicapi.leanapp.cn/abc/
  // baseURL: process.env.baseURL || process.env.apiUrl || ""
  timeout: 60 * 1000, // Timeout 超时时间 毫秒 1分钟超时..
  // withCredentials: true, // Check cross-site Access-Control
};
//创建对象
const _axios = axios.create(config);

//请求前置配置
_axios.interceptors.request.use(
  function(config) {
    //修改配置...
    // config.headers={
    //   TOKEN:"abc"
    // }
    // Do something before request is sent
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

//的到返回结果之后的后置处理.
_axios.interceptors.response.use(
  function(response) {
    // Do something with response data
    // console.log(response);
    // return response.data.result;
    return response;
  },
  function(error) {
    // Do something with response error
    return Promise.reject(error);
  }
);

Plugin.install = function(Vue) {
  Vue.axios = _axios;
  window.axios = _axios;
  //Object.defineProperty(Obj,'',{get(){}})
  //给Vue原型对象添加属性.
  Object.defineProperties(Vue.prototype, {
    axios: {
      get() {
        return _axios;
      }
    },
    $axios: {
      get() {//getter方法
        return _axios;
      }
    },
    $http:{
      get(){
        return _axios;
      }
    }
  });
};
//安装插件.
Vue.use(Plugin)

export default Plugin;
