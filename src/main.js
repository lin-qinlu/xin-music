import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import "@/assets/common.less"
import "@/assets/animate.min.css"

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
