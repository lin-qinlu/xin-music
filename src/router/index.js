import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect:"/recommend"
  },
  {
    path: '/recommend',
    name: 'Recommend',
    component: ()=>import('@/views/Recommend'),
    meta:{title:'',showNav:true}
  },
  {
    path: '/hot',
    name: 'Hot',
    component: ()=>import('@/views/Hot'),
    meta:{title:'',showNav:true}
  },
  {
    path: '/search',
    name: 'Search',
    component: ()=>import('@/views/Search'),
    meta:{title:'',showNav:true}
  },
  {
    path:'/playlist/:id',
    name:'PlayList',
    props:true,
    component:()=>import('../views/PlayList')
  },
  {
    path:'/mv/:id',
    name:'MV',
    props:true,
    /* meta:{

    }, */
    component:()=>import('@/views/MV'),
    meta:{title:'',mvComment:true}
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
